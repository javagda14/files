package pl.sda.pliki;

import java.io.*;

public class MainZapis {
    public static void main(String[] args) {

        try {
            PrintWriter writer = new PrintWriter("plik.txt");
            writer.println("Hello!");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


//        try(PrintWriter writer = new PrintWriter("plik.txt")){
//            writer.println("Dupa!");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }

        boolean working = true;

        while (working) {
            // co obieg pętli nadpisuje plik
            try (PrintWriter writer = new PrintWriter(new FileWriter("plik.txt", false))) {
                writer.println("Tekst!");
                writer.flush();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (PrintWriter writer2 = new PrintWriter(new FileWriter("plik.txt", false))) {
            while (working) {
                writer2.println("Tekst!");
                writer2.flush();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
