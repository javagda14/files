package pl.sda.generics.para;

import pl.sda.polimorfizm.Ptak;
import pl.sda.polimorfizm.Sowa;

public class Main {
    public static void main(String[] args) {
        Czlowiek czlowiek = new Czlowiek("Pan Bóg");
        Czlowiek ludzik = new Czlowiek("ludzik");


        Sowa bociek = new Sowa();

        Para<Czlowiek, Ptak> para = new Para<>(czlowiek, bociek);

        Czlowiek[] ludzie = new Czlowiek[10];
        Object[] obiekty = new Object[10];
        Ptak[] ptaki = new Ptak[10];
        Integer[] integers = new Integer[10];

        int[] integerss = new int[10];

        metoda(integers);

//        para.getLewy().

//        System.out.println(para);

    }

    // zadanie 1 - podstawowe
    public static <T> void metoda(T obiekt) {
        // wypisuje 10 razy zadany/podany w parametrze obiekt
        for (int i = 0; i < 10; i++) {
            System.out.println(obiekt);
        }
    }

    // Znaleźć wszystkie pary które są niepuste - zwracamy tablicę obiektów klasy !Para!
    // Jeśli w zbiorze 10 par 3 są niepuste to masz zwrócić 3 pary
    public static Para[] znajdzNiepuste(Para[] arg) {
        // musimy zliczyć ilość elementów niepustych żeby zaalokować tablicę odpowiednich rozmiarów
        // dlatego wykonujemy obieg pętli przez tablicę żeby sprawdzić jak dużą tablicę trzeba zaalokować
        int rozmiar = 0;
        for (Para para : arg) {
            if (para.czyNiepusta()) {
                rozmiar++;
            }
        }

        // deklarujemy indeks, pod który będziemy wstawiać elementy
        int indeksWstawiania = 0;

        // tworzymy tablicę Par o rozmiarze takim, jak zliczyliśmy u góry
        Para[] niepuste = new Para[rozmiar];
        for (Para para : arg) {
            // jeśli para jest niepusta
            if (para.czyNiepusta()) {
                // to wstawiam element do tablicy
                // oraz inkrementuje licznik - żeby następny element wstawić na nowe miejsce.
                niepuste[indeksWstawiania++] = para;
            }
        }

        return niepuste;
    }

    // zadanie 1 - rozwinięcie z *
    public static <T> void metodaVarArgs(T... obiekty) {
        // varargs to tablica
        for (T obiekt : obiekty) {
            // wypisuje każdy element 10 razy
            for (int i = 0; i < 10; i++) {
                System.out.println(obiekt);
            }
        }
    }

    public static <T> void metoda(T[] obiekty) {
        for (T obiekt : obiekty) {
            System.out.println(obiekt);
        }
    }
}
