package pl.sda.generics.para;

import pl.sda.polimorfizm.Ptak;

public class Para<P, L extends Ptak> {
    private P prawy;
    private L lewy;

    public Para(P prawy, L lewy) {
        this.prawy = prawy;
        this.lewy = lewy;
    }

    public P getPrawy() {
        return prawy;
    }

    public void setPrawy(P prawy) {
        this.prawy = prawy;
    }

    public L getLewy() {
        return lewy;
    }

    public void setLewy(L lewy) {
        this.lewy = lewy;
    }

    @Override
    public String toString() {
        return "Para{" +
                "prawy=" + prawy +
                ", lewy=" + lewy +
                '}';
    }

    public boolean czyNiepusta() {
        return lewy != null && prawy != null;
    }
}
