package pl.sda.generics.para;

public class Czlowiek {
    private String name;

    public Czlowiek(String name) {
        this.name = name;
    }

    public void przedstawSie(){
        System.out.println("Jam jest " + name);
    }

    @Override
    public String toString() {
        return "Czlowiek{" +
                "name='" + name + '\'' +
                '}';
    }
}
