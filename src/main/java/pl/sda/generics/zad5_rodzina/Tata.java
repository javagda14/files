package pl.sda.generics.zad5_rodzina;

public class Tata implements ICzlonekRodziny {
    @Override
    public void przedstawSie() {
        System.out.println("Jestem ojciec!");
    }

    public void zawolajPoPiwo(){
        System.out.println("Przynieś piwko dziecko!");
    }
}
