package pl.sda.generics.zad5_rodzina;

public class Mama implements ICzlonekRodziny {
    @Override
    public void przedstawSie() {
        System.out.println("Jestem matka!");
    }
}
