package pl.sda.generics.zad5_rodzina;

public class Syn implements ICzlonekRodziny {
    @Override
    public void przedstawSie() {
        System.out.println("Masz piątaka?!");
    }

    @Override
    public boolean czyJestDorosly() {
        return false;
    }
}