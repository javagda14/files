package pl.sda.generics.zad5_rodzina;

public interface ICzlonekRodziny {
    public void przedstawSie();

    public default boolean czyJestDorosly(){
        return true;
    }
}
