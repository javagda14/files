package pl.sda.generics.zad5_rodzina;

public class Corka implements ICzlonekRodziny {
    @Override
    public void przedstawSie() {
        System.out.println("Jestem córka!");
    }

    @Override
    public boolean czyJestDorosly() {
        return false;
    }
}
