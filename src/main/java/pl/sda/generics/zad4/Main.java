package pl.sda.generics.zad4;

import pl.sda.generics.para.Czlowiek;

public class Main {
    public static void main(String[] args) {
        Czlowiek czlowiek = new Czlowiek("marian");
        sumaLiczb(123L, 5 );
    }

    public static <T extends Number> Double sumaLiczb(T liczba1, T liczba2) {
        double wynik = liczba1.doubleValue() + liczba2.doubleValue();
        return wynik;
    }
}
