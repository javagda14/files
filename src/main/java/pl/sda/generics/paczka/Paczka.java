package pl.sda.generics.paczka;

// zadanie 2
public class Paczka<T> {
    private T obiektWPaczce;

    public Paczka(T obiektWPaczce) {
        this.obiektWPaczce = obiektWPaczce;
    }

    public T getObiektWPaczce() {
        return obiektWPaczce;
    }

    public void setObiektWPaczce(T obiektWPaczce) {
        this.obiektWPaczce = obiektWPaczce;
    }

    public boolean czyPudelkoJestPuste() {
        return obiektWPaczce == null;
    }
}
