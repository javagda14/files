package pl.sda.generics.optional;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Biblioteka biblioteka = new Biblioteka();

        Optional<Ksiazka> pudelko = biblioteka.getKsiazka("Qvo vadis");
        if(pudelko.isPresent()) {
            Ksiazka ksiazka = pudelko.get();
            System.out.println(ksiazka.getAuthorsName());
        }

        Optional<String> pudelkoDrugie = biblioteka.getAuthorsName("Qvo vadis");
        if(pudelkoDrugie.isPresent()) {
            String authorsName = pudelkoDrugie.get();
            System.out.println(authorsName);
        }
    }
}
