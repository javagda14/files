package pl.sda.generics.optional;

import java.util.Optional;

public class Biblioteka {
    private Ksiazka[] ksiazkas = new Ksiazka[300];

    public Optional<Ksiazka> getKsiazka(String title) {
        for (int i = 0; i < ksiazkas.length; i++) {
            if(ksiazkas[i].getTytul().equals(title)){
                return Optional.of(ksiazkas[i]); // pełne opakowanie
            }
        }
        return Optional.empty(); // puste opakowanie
    }

    public Optional<String> getAuthorsName(String title) {
        for (int i = 0; i < ksiazkas.length; i++) {
            if(ksiazkas[i].getTytul().equals(title)){
                return Optional.of(ksiazkas[i].getAuthorsName()); // pełne opakowanie
            }
        }
        return Optional.empty(); // puste opakowanie
    }
}
