package pl.sda.polimorfizm;

public class Main {
    public static void main(String[] args) {
        // Mając kilka poziomów abstrakcji (klasy dziedziczące oraz klasy dziedziczone)
        // wywołanie metody uzależnione jest od typu referencji którą przechowujemy w zmiennej.
        // Jeśli posiadamy referencję typu Ptak (klasa wyższego poziomu) i wywołujemy na obiekcie
        // metodę śpiewaj() która jest przeciążona w klasach dziedziczących, to wywołana zostanie
        // metoda z tej klasy końcowej, do której posiadamy referencję (tutaj referencja - > new Bocian(); )
        Ptak ptak = new Bocian();

        ptak.spiewaj();
        if(ptak instanceof Sowa) {
            Sowa sowa = (Sowa) ptak;
            sowa.powiedzCos();
        }
    }
}
